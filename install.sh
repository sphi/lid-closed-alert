#!/bin/bash
set -euox pipefail

which ffplay || (echo "ffplay not installed"; exit 1)
which amixer || (echo "amixer not installed"; exit 1)

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cp "$DIR/lid-closed-alert.sh" "$HOME/.config"
"$HOME/.config/lid-closed-alert.sh" --test

mkdir -p "$HOME/.config/systemd/user/"
cp "$DIR/lid-closed-alert.service" "$HOME/.config/systemd/user/"

systemctl --user daemon-reload
systemctl enable --user lid-closed-alert
systemctl restart --user lid-closed-alert
systemctl status --user lid-closed-alert
