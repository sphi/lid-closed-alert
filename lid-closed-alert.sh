#!/bin/bash
set -euo pipefail

# This script will continuously check if a laptop lid is closed. If it remains closed for at
# least $WAIT_TIMEOUT seconds, it will attempt to suspend the system. If that doesn't work it
# will set volume to 100%, play an alert tone, and then restore volume to previous level. It
# will continue to do this as long as the lid is closed. If the laptop goes to sleep, this
# script (along with everything else) will be suspended, and no alert will play.

# How often to check if the lid is closed
CHECK_INTERVAL=20
# Once we detect the lid is closed, how long to wait before alerting
WAIT_TIMEOUT=5

play_tone() {
  ffplay -f lavfi -i "sine=frequency=800:duration=0.8" -autoexit -nodisp &>/dev/null
  sleep 0.5
}

play_alert() {
  # Record current audio settings
  WAS_MUTED="$(amixer sget Master | grep '\[off\]' || true)"
  WAS_LEVEL="$(amixer sget Master | grep -Po '(?<=\[)\d+%(?=\])' | head -n 1)"
  # Set to full volume
  amixer sset Master 100% &>/dev/null
  amixer sset Master unmute &>/dev/null
  # Play alert
  for i in {1..4}; do
    play_tone
  done
  # Restore previous audio settings
  echo "Restoring audio ($WAS_LEVEL, $(if test -z "$WAS_MUTED"; then echo "not "; fi)muted)"
  amixer sset Master "$WAS_LEVEL" &>/dev/null
  if test -z "$WAS_MUTED"; then
    amixer sset Master unmute &>/dev/null
  else
    amixer sset Master mute &>/dev/null
  fi
}

check_lid() {
  if cat /proc/acpi/button/lid/LID/state | grep open >/dev/null; then
    return 0
  elif test "$(SWAYSOCK="$(ls /run/user/$UID/sway-ipc*.sock | head -n 1)" swaymsg -t get_outputs | jq length)" -gt 1; then
    return 0
  else
    return 1
  fi
}

if test "${1:-}" == "--test"; then
  echo "Testing..."
  check_lid
  echo "Lid closed: $?"
  read -p "Play test alert (loud)? [Y/n] " PLAY
  if test "$PLAY" != 'n'; then
    play_alert
  fi
  echo "Done"
  exit 0
fi

echo "Starting..."
while true; do
  if ! check_lid; then
    echo "Lid closed, waiting $WAIT_TIMEOUT seconds..."
    sleep "$WAIT_TIMEOUT"
    if ! check_lid; then
      echo "Lid still closed, suspending..."
      systemctl suspend
      sleep "$WAIT_TIMEOUT"
      if ! check_lid; then
        echo "Lid STILL closed! playing alert ($(date))"
        play_alert
      else
        echo "Lid open, ignoring"
      fi
    else
      echo "Lid open, ignoring"
    fi
  fi
  sleep "$CHECK_INTERVAL"
done
