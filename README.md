Sometimes you close your lid of your Linux laptop, and it does not go to sleep. It would be great if this didn't happen, but the next best thing is to know about it. This script and systemd service will play a tone at max volume when the lid is closed but the laptop is not asleep. To get it running, simply run `install.sh`.

Note: this is a quickly slapped together BASH script. Please review the code before using. I am not responsible for any damage this may directly or indirectly cause to your computer.
